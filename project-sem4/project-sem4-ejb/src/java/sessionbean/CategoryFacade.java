/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entity.Category;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Khoi
 */
@Stateless
public class CategoryFacade extends AbstractFacade<Category> implements CategoryFacadeLocal {
    @PersistenceContext(unitName = "project-sem4-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoryFacade() {
        super(Category.class);
    }
        @Override
    public Category findById(int id) {
        Query query = em.createNamedQuery("Category.findByCateId");
        query.setParameter("cateId", id);
        Category category = (Category) query.getSingleResult();
        return category;
    }

    @Override
    public boolean findByName(String name) {
        try {
            Query query = em.createNamedQuery("Category.findByCategoryName");
            query.setParameter("categoryName", name);
            Category category = (Category) query.getSingleResult();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
}
