
window.onload = function() {
  // Get the Sidenav
  var mySidenav = document.getElementById("mySidenav");

  // Get the DIV with overlay effect
  var overlayBg = document.getElementById("myOverlay");

  // Toggle between showing and hiding the sidenav, and add overlay effect
  function w3_open() {
      if (mySidenav.style.display === 'block') {
          mySidenav.style.display = 'none';
          overlayBg.style.display = "none";
      } else {
          mySidenav.style.display = 'block';
          overlayBg.style.display = "block";
      }
  }

  // Close the sidenav with the close button
  function w3_close() {
      mySidenav.style.display = "none";
      overlayBg.style.display = "none";
  }

  function hide_nortification() {
    $(".nortification-table").css("opacity", "0");
    setTimeout(function() {$(".nortification-table").css("visibility", "hidden");}, 1000);
  }

  function show_nortification() {
      $(".nortification-table").css("visibility", "visible");
      $(".nortification-table").css("opacity", "1");
  }

  function add_nortification() {
    $(".nortification-table tbody").append(
      '<tr> \
        <td><i class="fa fa-user w3-blue w3-padding-tiny"></i></td> \
        <td>New record, over 90 views.</td> \
      </tr> \
    ');
  }

  $("#nortification-btn").click( function() {
    if( $(".nortification-table").css("opacity") == 0 ) {
      show_nortification();
    } else {
      hide_nortification();
    }
  } );

  show_nortification();
  setTimeout(hide_nortification, 3000);
  $("#open_nav_btn").click(w3_open);

  // Load the Visualization API and the corechart package.
  google.charts.load('current', {'packages':['line']});
  // Set a callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawChart);

  // Callback that creates and populates a data table,
  // instantiates the pie chart, passes in the data and
  // draws it.
  function drawChart() {

    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'tháng');
    data.addColumn('number', 'sản phẩm');
    data.addRows([
      ["Tháng 1", 10],
      ["Tháng 2", 30],
      ["Tháng 3", 50],
      ["Tháng 4", 10],
      ["Tháng 5", 20],
      ["Tháng 6", 50],
      ["Tháng 7", 70],
      ["Tháng 8", 50],
      ["Tháng 9", 10],
      ["Tháng 10", 30],
      ["Tháng 11", 70],
      ["Tháng 12", 30]
    ]);
    // Set chart options
    var options = {
        chart: {
          title: 'Doanh thu bán hàng',
          subtitle: 'đơn vị(x 1 sản phẩm)'
        },
        width: "100%",
        height: 300
      };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.charts.Line(document.getElementById('chart_div'));

    chart.draw(data, options);
  }
}
