/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionbean;

import entity.Category;
import entity.Product;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.Part;

/**
 *
 * @author Khoi
 */
@Stateless
public class ProductFacade extends AbstractFacade<Product> implements ProductFacadeLocal {

    @PersistenceContext(unitName = "project-sem4-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductFacade() {
        super(Product.class);
    }

    @Override
    public Product findById(int id) {
        Query query = em.createNamedQuery("Product.findByPId");
        query.setParameter("pId", id);
        Product product = (Product) query.getSingleResult();
        return product;
    }

    @Override
    public boolean findByName(String name) {
        try {
            Query query = em.createNamedQuery("Product.findByProductName");
            query.setParameter("productName", name);
            Product product = (Product) query.getSingleResult();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean AddProduct(Category category, String proname, String imglink, double price, int quantity, String des) {
        if (findByName(proname)) {
            return false;
        } else {
            try {
                Product product = new Product();
                product.setCateId(category);
                product.setProductName(proname);
                product.setImageLink(imglink);
                product.setProductPrice(price);
                product.setQuantity(quantity);
                product.setDescription(des);
                em.persist(product);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    @Override
    public boolean EditProduct(int id, Category category, String proname, double price, int quantity, String des) {
        try {
            Product product = findById(id);
            product.setCateId(category);
            product.setProductName(proname);
            product.setProductPrice(price);
            product.setQuantity(quantity);
            product.setDescription(des);
            em.merge(product);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean remove(int id) {
        try {
            Product product = findById(id);
            if (product == null) {
                return false;
            } else {
                em.remove(product);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

}
